import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClientService } from '../services/client.service';

@Component({
  selector: 'app-client-registration',
  templateUrl: './client-registration.component.html',
  styleUrls: ['./client-registration.component.scss']
})
export class ClientRegistrationComponent implements OnInit {

  registrationForm: FormGroup;
  error: any;
  validMessage: string = "";
  submitted = false;

  constructor(private clientService: ClientService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.registrationForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      surname: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      address: ['', [Validators.required]],

    });


  }

  get f() { return this.registrationForm.controls; }

  submitClientRegistration() {
    this.submitted = true;
    if (this.registrationForm.valid) {

      this.validMessage = "Your registration had been submitted.Thank You!"
      this.clientService.createClient(this.registrationForm.value).subscribe(
        data => {
          this.registrationForm.reset();
          return true;

        },
        error => {
          return this.error = error;
        }
      )

    } else {
      this.validMessage = "Please fill out the form before submitting!"


    }

  }


}
