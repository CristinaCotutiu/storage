import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { ClientRegistrationComponent } from './client-registration/client-registration.component';
import { ClientlistComponent } from './clientlist/clientlist.component';
import { UnitsComponent } from './units/units.component';

const routes: Routes = [
  { path: "clients", component: ClientlistComponent },
  { path: "units", component: UnitsComponent },
  { path: "about", component: AboutComponent },
  { path: "client/registration", component: ClientRegistrationComponent }

];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
