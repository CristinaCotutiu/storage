import { Component, OnInit } from '@angular/core';
import { ClientService } from '../services/client.service';


@Component({
  selector: 'app-clientlist',
  templateUrl: './clientlist.component.html',
  styleUrls: ['./clientlist.component.scss']
})
export class ClientlistComponent implements OnInit {

  public clients;

  constructor(private clientService: ClientService) { }
  getClients() {
    this.clientService.get().subscribe(
      data => { this.clients = data },
      err => console.error(err),
      () => console.log('clients loaded')
    );
  }
  ngOnInit(): void {
    this.getClients();
  }

}
