import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

const httpOptions = {
  headers: new HttpHeaders({ 'content-type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class UnitService {

  constructor(private http: HttpClient) { }

  //GET
  get() {
    return this.http.get('/server/units');
  }
  //GET BY ID
  getById(id: number) {
    return this.http.get('/server/units/unit/' + id);
  }

  //POST
  create(unit) {
    let body = JSON.stringify(unit);
    return this.http.post('/server/units/unit', body, httpOptions);
  }
}
