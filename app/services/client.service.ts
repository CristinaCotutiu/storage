import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'content-type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) { }

  //GET
  get() {
    return this.http.get('/server/clients');
  }
  //GET BY ID
  getById(id: number) {
    return this.http.get('/server/clients/client/' + id);
  }

  //POST
  createClient(client) {
    let body = JSON.stringify(client);
    return this.http.post('/server/clients/client', body, httpOptions);
  }

}

