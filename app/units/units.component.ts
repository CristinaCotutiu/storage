import { Component, OnInit } from '@angular/core';
import { UnitService } from '../services/unit.service';

@Component({
  selector: 'app-units',
  templateUrl: './units.component.html',
  styleUrls: ['./units.component.scss']
})
export class UnitsComponent implements OnInit {

  public units;

  constructor(private unitService: UnitService) { }
  getUnits() {
    this.unitService.get().subscribe(
      data => { this.units = data },
      err => console.error(err),
      () => console.log('units loaded')
    );
  }
  ngOnInit(): void {
    this.getUnits();
  }


}
